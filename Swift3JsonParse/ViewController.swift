//
//  ViewController.swift
//  Swift3JsonParse
//
//  Created by Batuhan GÖKTEPE on 29.06.2017.
//  Copyright © 2017 Batuhan GÖKTEPE. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var baslik = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        // [] -> Array
        // {} -> Object/Nesne - Dictinary
        
        let urlString = "http://bucayapimarket.com/json.php"
        let url = URL(string: urlString)
        
        let task = URLSession.shared.dataTask(with: url!) { (data,response,error) in
        
            if error != nil { //Hata varsa
            
            print(error!)
            
            }
            else {
            
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSArray
                    
                    
                    if let jsonDic = json {
                    
                    
                        for i in 0..<jsonDic.count {
                        
                            if let basliklar = jsonDic[i] as? NSDictionary
                            {
                            
                            self.baslik.append(basliklar["baslik"] as? NSString as! String)

                            print(basliklar["baslik"] as? NSString ?? "Baslik bulunamadı")
                            
                            }
                        
                        }
                        print(self.baslik)
                    
                    }

                    /*
                    for i in 0...5 {
                        
                        let jsonDic = json[i] as! NSDictionary

                        print(jsonDic["baslik"] as! NSString)

                    }
                    */
                    
                    
                    }
                    
                
                catch{
                
                    print(error)
                
                }
            
            
            }
        
        }
        task.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

